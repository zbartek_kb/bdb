var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-minify');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');


gulp.task('sass-styles', function(){
    return gulp.src('app/assets/scss/main.scss')
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(minifyCSS())
        .pipe(concat('main.css'))
        .pipe(gulp.dest('app/assets/css'))
});
gulp.task('watch',['sass-styles'],function () {
        return gulp.watch(['app/assets/scss/*.scss','app/assets/css/main.css'],['sass-styles']);
})
gulp.task('default', [ 'sass-styles' ]);